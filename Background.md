## Contents
- [Architecture Background](#architecture-background)
	- [Problem Background](#problem-background)
		- [System Overview](#system-overview)
		- [Context](#context)
		- [Driving Requirements](#driving-requirements)
			- [Functional requirements](#functional-requirements)
			- [Quality attributes](#quality-attributes)
				- [Funcionalidade](#funcionalidade)
				- [Usabilidade](#usabilidade)
				- [Confiabilidade (Reliability)](#confiabilidade-reliability)
				- [Desempenho (Performance)](#desempenho-performance)
				- [Suportabilidade](#suportabilidade)
				- [Design constraints](#design-constraints)
				- [Implementation constraints](#implementation-constraints)
				- [Interface constraints](#interface-constraints)
				- [Physical constraints](#physical-constraints)
	- [Solution Background](#solution-background)
		- [Architectural Approaches](#architectural-approaches)
		- [Analysis Results](#analysis-results)
		- [Mapping Requirements to Architecture](#mapping-requirements-to-architecture)

# Architecture Background
>Architecture Background provides information about the software architecture, by:
>- describing the background and rationale for the software architecture;
>- explaining the constraints and influences that led to the current architecture;
>- describing the major architectural approaches that have been utilized in the architecture.
  
## Problem Background

### System Overview

Permite:

- Simular uma rede social;
- Permite o jogador expandir a sua rede social, estabelecer ligações passando de missão em missão e subindo no leaderboard;
- Vizualizar essa mesma rede social;


### Context

A Graphs4Social, S.A. é uma startup com sede no Porto (Portugal) cuja missão é fornecer aplicações
de manipulação e visualização de grafos de redes sociais. A empresa decidiu recentemente expandir o
sue portfolio de produtos entrando na área de jogos, mas mantendo o foco nos grafos de redes sociais.
A empresa decidiu recorrer à subcontratação de serviços de desenvolvimento uma vez que não possui
capacidade livre de momento

O objectivo seria então o desenvolvimento de um protótipo para um jogo baseado na visualização e manipulação
de grafos sociais.

O Jogo simula uma rede social e o jogador tem por objetivo expandir a sua rede social, com o objetivo
último de ter a maior e mais forte rede social possível. O jogo desenrola-se numa série de missões em
que o jogador terá que avançar e aumentar a sua rede para subir no leader board. Um utilizador pode
iniciar um jogo/missão em qualquer momento escolhendo.

As missões consistem em tornar-se amigo de outro utilizador separado por n graus de distância, em
que n será tanto maior quanto maior a dificuldade do nível. O sistema escolherá alguém que esteja a
essa distância (tendo por base as tags e conexões partilhadas) e depois o jogador terá que ir jogando
para se fazer “amigo” de cada nó desse percurso até chegar à pessoa desejada. Uma missão bem
concluída significa que o utilizador consegue criar uma ligação direta com o utilizador objetivo que se
encontrava a n níveis de distância.

Fonte: [RFP para Desenvolvimento jogo social-LAPR5-21-22.v7b.pdf] (https://moodle.isep.ipp.pt/pluginfile.php/147597/mod_resource/content/12/RFP%20para%20Desenvolvimento%20jogo%20social-LAPR5-21-22.v7b.pdf)

Este SAD serve de base para debate sobre o sistema a construir (a implementar, testar e implantar), e pretende-se que esteja alinhado com o sistema construído. Para além do óbvio na descrição duma arquitetura de software, deve identificar alternativas de design e ponto de variação.

### Driving Requirements
> This section lists the functional requirements, quality attributes and design constraints. It may point to a separate requirements document.

#### Functional requirements
##### Sprint A
ID	UC		Description
1.			Planear o esqueleto da aplicação. Identificar que módulos e interfaces (detalhadas) de cada módulo. Identificar diferenças de terminologia, se existirem, entre o vários dominios.
2. 	UC9: 	Escolher quais “utilizadores objetivo” (sugeridos pelo sistema) o jogador recém registado gostaria de ter na sua rede.
3. 	UC11: 	Pedir introdução a utilizador objetivo.
4. 	UC12: 	Aprovar/desaprovar pedido de introdução.
5. 	UC3: 	Editar relacionamento com tags e força de ligação.
6.  UC5:	Editar perfil próprio.
7.  UC6: 	Editar estado de humor.
8. 	UC7: 	Consultar a rede a partir da sua perspetiva.
9.	UC8: 	Registar utilizador no sistema.
10. UC10: 	Pesquisar utilizadores que conheça na rede, e pedir ligação de utilizador.
11. UC35: 	Obter lista de pedidos de ligação pendentes
12. UC33: 	Aceitar ou rejeitar a introdução
13. 		Setup dos projetos e repositórios Git (Bitbucket)
14. 		Design arquitetural:
 14.1 		Nível 1: Vista lógica e de cenários
 14.2 		Nível 2: Vista lógica, de processo, de implementação e física
 14.3 		Nível 3: (Master Data Rede): vista lógica, de processo e de implementação
 14.4 		Adoção de estilos/padrões: cliente-servidor, SOA, DDD, Onion (inclui DI, IoC), DTO, ...
15. 		Tecnologia: .Net C#, SGBD relacional (e.g. MS SQL Server), ORM (e.g. Entity Framework)”.
16. 		Testes unitários e de integração (com isolamento via mocks)
17. 		Implantação na cloud (e.g. Heroku, MongoDB Atlas)
18. 		Pipelines (Bitbucket Pipelines)
20. 		Como data administrator, quero listar serviço de tripulante num determinado dia.

[...]

##### Sprint B
[...]

##### Sprint C
[...]

<inserir aqui o modelo de casos de uso/>

#### Quality attributes
Os atributos de qualidade são categorizados e sistematizados segundo o modelo [FURPS+](https://pt.wikipedia.org/wiki/FURPS).

##### Requisitos de Qualidade (RNF)

RQ 1: Pretende-se uma solução integrada, um sistema, englobando vários módulos interligados entre si.
RQ 3: O protó@po inicial deve ser cons@tuído pelos seguintes módulos:
• Jogo 3D com componentes de inteligência ar@ficial
• Gestão de rede social e dados mestres
• Leaderboard e consultas
RQ 8: As aplicações devem possuir uma organização em camadas separando os componentes de apresentação (interface pessoa-máquina) dos
componentes de processamento e acesso a dados recorrendo às boas prá@cas referenciadas nos padrões de aplicações empresariais.
RQ 9: Na descrição da arquitetura, evidenciar como é que os vários módulos do sistema se encontram integrados [...], a entregar será uma descrição da
arquitetura do sistema onde constem:
• Os módulos existentes
• As interfaces entre módulos (devidamente especificadas)
• O modelo de domínio de cada módulo incluindo glossário [...].
RQ 10: O módulo de navegação e visualização 3D deverá permi@r a visualização em 3D do grafo da rede social do u@lizador. Deverá também [...].
RQ 11: O sistema deve possuir um módulo de inteligência ar@ficial desenvolvido em Prolog.
RQ 13: O módulo Site deve consis@r numa aplicação web cuja principal função será o registo de u@lizadores, permi@r ao u@lizador construir a sua rede
social [...].
RQ 14: O acesso ao módulo Site está disponível para u@lizadores registados e cada u@lizador tem apenas acesso aos seus dados e da sua rede social.
RQ 15: Todos os u@lizadores (estejam ou não auten@cados) podem [...].
RQ 16: Os u@lizadores auten@cados podem [...].


De acordo com o disponibilizado em [link] (https://moodle.isep.ipp.pt/pluginfile.php/147499/mod_resource/content/2/ARQSI%202021-2022%20Introdu%C3%A7%C3%A3o%20ao%20projeto.pdf)

##### Funcionalidade
1. Cada sistema só poderá aceder aos dados que lhe dizem respeito.
2. Deve ser auditada e verificada a integridade da informação a que os sistemas acedem.
3. Com vista à necessidade de saber e necessidade de conhecer, toda a informação deve estar protegida de acessos indevidos. Ou seja, o princípio de minimização de acesso ao que é essencial para cada utilizador/aplicação, criação de túneis para transferência de informação, avaliação da integridade de dados e aplicações, e encriptação/minimização dos dados.
4. Uma vez que o módulo de gestão de encomendas se encontra virado para o exterior, é necessário ter especial atenção com a privacidade e proteção de dados à luz do RGPD. Assim é necessário que o sistema cumpra a legislação em vigor e, em especial, disponibilize as informações legais e informe o utilizador aquando do seu registo, bem como permita aceder e cancelar a sua conta nos casos e nas condições legalmente permitidas.

##### Usabilidade
5. A SPA deve permitir acesso a todos os módulos do sistema: master data, planeamento e visualização, bem como RGPD.

6.  No âmbito do projeto atual, a administração de utilizadores pode ser efetuada diretamente na base de dados não sendo necessário um módulo de gestão de utilizadores.

##### Confiabilidade (Reliability)
n/a

##### Desempenho (Performance)
n/a

##### Suportabilidade
A TRABALHAR/EDITAR...
7. Embora não esteja no âmbito atual do projeto, deve ser levado em conta na arquitetura da solução, a extensão futura para aplicações móveis.

##### Design constraints
A TRABALHAR/EDITAR...
8. O sistema deve ser composto por uma aplicação web do tipo Single Page Application (SPA) que permite aos utilizadores autorizados acederem aos diferentes módulos da aplicação, bem como por um conjunto de serviços que implementem as componentes de regras de negócio necessárias para o funcionamento da aplicação web.

![Visão geral do sistema definido no enunciado/caderno de encargos](diagramas/visaogeralsistema_enunciado.png)


<img src="diagramas/visaogeralsistema_enunciado.png" width="75%">

De um modo geral, as principais funcionalidades de cada módulo são as seguintes:

- Master data – permite a gestão da informação relacionada com a rede (nós, percursos), tipos de viaturas, tipos de tripulantes, linhas e viagens.
- Planeamento – com base nos percursos existentes planear as trocas de tripulações nos pontos de rendição. Planear os serviços de tripulantes com base nos serviços de viatura. Consome a informação gerida no módulo master data e publica informação do planeamento para o módulo de visualização.
- Visualizador 3D –  permite a visualização 2D e 3D da rede, a navegação pela cena e a consulta gráfica de informação sobre as viagens. Consome a informação gerida no módulo master data e no módulo
- UI – interface com o utilizador
- Clientes + RGPD – gestão de informação dos utilizadores finais “clientes” e seus consentimentos no âmbito do RGPD


##### Implementation constraints
11.   Todos os módulos devem fazer parte do código fonte da mesma SPA e serem disponibilizados como um único artefacto.

##### Interface constraints
A TRABALHAR/EDITAR...
12.  A SPA deve permitir acesso a todos os módulos do sistema: master data, planeamento e visualização, bem como RGPD. (repetida porque diz respeito a duas categrorias)
13.  O módulo de Planeamento deve consumir dados de rede através da API do master data
14.  O módulo de Planeamento deve consumir dados de viagens através da API do master data
15.  O módulo de Visualização deve consumir dados de rede através da API do master data
16.  O módulo de Visualização deve consumir dados de viagens através da API do master data "viagens"
17.  O módulo de Visualização deve consumir dados de serviços de tripulante através da API do planeamento

[...]

##### Physical constraints
A TRABALHAR/EDITAR...
18. Existem dois servidores em load balancing, onde estão instaladas as aplicações, serviços e as bases de dados e que se encarregam do armazenamento da informação.

19. Existem ainda dois servidores em failover que distribuem os endereços a todos os sistemas e se encarregam da autenticação de sistemas e utilizadores (DHCP, DNS (se aplicável) e autenticação de servidores, e eventualmente um servidor Kerberos).
20. Algumas das aplicações devem ser implantadas *on premises* e outras em IaaS e PaaS (*on cloud*). Cf. requisitos específicos das UC por sprint.

## Solution Background
A TRABALHAR/EDITAR...
> The sub-parts of this section provide a description of why the architecture is the way that it is, and a convincing argument that the architecture is the right one to satisfy the behavioral and quality attribute goals levied upon it.

### Architectural Approaches
A TRABALHAR/EDITAR...
> This section provides a rationale for the major design decisions embodied by the software architecture. It describes any design approaches applied to the software architecture, including the use of architectural styles or design patterns, when the scope of those approaches transcends any single architectural view. The section also provides a rationale for the selection of those approaches. It also describes any significant alternatives that were seriously considered and why they were ultimately rejected. The section describes any relevant COTS issues, including any associated trade studies.

Baseado nos requisitos não funcionais e restrições de design, serão adotadas as seguintes abordagens/padrões/estilos:

- Client-Server, porque cada um dos "módulos" MDR, MDV, Planeamento são aplicações servidoras de outras aplicações clientes (e.g. MDR é servidor de MDV e UI, MDV é servidor de Planeamento e UI, e Planeamento é servidor de UI);
- Web Application, em que o frontend é desempenhado por uma SPA (Single Page Application), e que o backend é desempenhado pelos módulos MDR, MDV e Planeamento;
- SOA, porque os servidores (cf. anterior) deverão disponibilizar API, e particularmemte API para serem usadas na web, disponibilizados serviços para os clientes respetivos. Serão adotados os nível 0, 1 e 2 do [Modelo de Maturidade de Richardson](https://martinfowler.com/articles/richardsonMaturityModel.html) aplicado a REST;
- N-Tier, pois as várias aplicações devem ser implantadas em diferentes máquinas *on premises* e IaaS e PaaS (*on cloud*), de acordo com os requisitos não funcionais;
- Layered architecture, mais especificamente Onion Architecture, por razões académicas.

Outras abordagens/estilos/padrões, como e.g. interligação entre aplicações baseado em mensagens-eventos foram desconsideradas para não violar os requisitos e restrições definidos, mas também por questões académicas.

### Analysis Results
A TRABALHAR/EDITAR...
> This section describes the results of any quantitative or qualitative analyses that have been performed that provide evidence that the software architecture is fit for purpose. If an Architecture Tradeoff Analysis Method evaluation has been performed, it is included in the analysis sections of its final report. This section refers to the results of any other relevant trade studies, quantitative modeling, or other analysis results.

Não existem por agora resultados de análise ou avaliação. Estudos qualitativos acerca dos estilos/padrões adotados (nomeadamente Onion em MDR e MDV, mas também Dependency Injection na UI), permitem empiricamente advogar que a manutenibilidade, evolutabilidade e testabilidade do software são elevadas, ao mesmo tempo que permitem atingir as funcionalidades desejadas.

A colocar noutra página:


### Mapping Requirements to Architecture
A TRABALHAR/EDITAR...
> This section describes the requirements (original or derived) addressed by the software architecture, with a short statement about where in the architecture each requirement is addressed.

TBD