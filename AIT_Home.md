# AIT

Esta parte da wiki consiste no Software Architecture Document (SAD)/Documento de Arquitetura de Software (DAS) da aplicação requisitada pela Graphs4Social, S.A., e desenvolvida no contexto do 5º semestre da LEI em 2021-2022 com o apoio da mesma empresa.

Este DAS é constituído por:

- [Documentation Roadmap and Overview](RoadmapOverview.md)
- [Architecture Background](Background.md)
- [Views](Views.md)
- [Mapping Between Views](Mapping.md)
- [Referenced Materials](References.md)
- [Glossary and Acronyms](Gloassary&Acronyms.md)


NB: Este DAS é adaptado de [Modelo+exemplar+fornecido+no+contexto+das+aulas+de+ARQSI](https://bitbucket.org/nunopsilva/bulletproof-nodejs-ddd/wiki/) que por sua vez foi baseado em [Software+Architecture+Documentation+Template](https://wiki.sei.cmu.edu/confluence/display/SAD/Software+Architecture+Documentation+Template)