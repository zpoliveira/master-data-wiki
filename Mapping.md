- [Mapping between Views](#mapping-between-views)
	- [Nível 1](#nível-1)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física)
	- [Nível 2](#nível-2)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação-1)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física-1)
	- [Nível 3 (MDR)](#nível-3-mdr)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação-2)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física-2)
	- [Nível 3 (UI)](#nível-3-ui)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação-3)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física-3)
	- [Nível 3 (MDV)](#nível-3-mdv)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação-4)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física-4)
	- [Nível 3 (Planeamento)](#nível-3-planeamento)
		- [Vista Lógica - Vista de Implementação](#vista-lógica---vista-de-implementação-5)
		- [Vista de Implementação - Vista Física](#vista-de-implementação---vista-física-5)

# Mapping between Views


## Nível 1
### Vista Lógica - Vista de Implementação

![N1-VistaLogica](diagramas/nivel1/N1-VistaLogica.png)

### Vista de Implementação - Vista Física

![N1-VP-USa-LoginJogador](diagramas/nivel1/N1-VP-USa-LoginJogador.png)

![N1-VP-US03](diagramas/nivel1/N1-VP-US05-AtualizaPerfil.png)

![N1-VP-US05-AtualizaPerfil](diagramas/nivel1/N1-VP-US05-AtualizaPerfil.png)

![N1-VP-US08-RegistarJogador](diagramas/nivel1/N1-VP-US08-RegistarJogador.png)

![N1-VP-US35-ObterListaPedidosLigacaoPendentes](diagramas/nivel1/N1-VP-US35-ObterListaPedidosLigacaoPendentes.png)

## Nível 2
### Vista Lógica - Vista de Implementação 

![N2-VLxVI](diagramas/nivel2/N2-VLxVI.png)

### Vista de Implementação - Vista Física
![N2-VIxVF](diagramas/nivel2/N2-VIxVF.png)

## Nível 3 (MDR)
### Vista Lógica - Vista de Implementação
![N3-VI-MDR-alt1](diagramas/nivel3/MDR/N3-VI-MDR-alt1.png)

![N3.1-VI-MDR-alt2](diagramas/nivel3/MDR/N3.1-VI-MDR-alt2.png)

### Vista de Implementação - Vista Física

####US03
![N3-VP-US03](diagramas/nivel3/N3-VP-US03.png)

####US08
![N3-VP-US08](diagramas/nivel3/N3-VP-US08.png)

####US09
![N3-VP-US09](diagramas/nivel3/N3-VP-US09.png)

####US35
![N3-VP-US35](diagramas/nivel3/N3-VP-US35.png)


## Nível 3 (UI)
### Vista Lógica - Vista de Implementação
TBD

### Vista de Implementação - Vista Física
TBD

## Nível 3 (MDV)
### Vista Lógica - Vista de Implementação
TBD

### Vista de Implementação - Vista Física
TBD

## Nível 3 (Planeamento)
### Vista Lógica - Vista de Implementação
TBD

### Vista de Implementação - Vista Física
TBD