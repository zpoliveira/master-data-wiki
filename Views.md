## Contents
- [Views](#views)
	- [Introduction](#introduction)
	- [Nível 1](#nível-1)
		- [Vista Lógica](#vista-lógica)
		- [Vista de Processos](#vista-de-processos)
			- [SSD US08](#n1-ssd-us08)
			- [SSD US05](#n1-ssd-us05)
			- [SSD Login](#n1-ssd-login)
	- [Nível 2](#nível-2)
		- [Vista Lógica](#vista-lógica-1)
		- [Vista de Processos](#vista-de-processos-1)
			- [SSD US03](#ssd-us03)
			- [SSD US05](#ssd-us05)
			- [SSD US06](#ssd-us06)
			- [SSD US07](#ssd-us07)
			- [SSD US08](#ssd-us08)
			- [SSD US09](#ssd-us09)
			- [SSD US10](#ssd-us10)
			- [SSD US11](#ssd-us11)
			- [SSD US12](#ssd-us12)
			- [SSD US33](#ssd-us33)
			- [SSD US35](#ssd-us35)
                        - [SSD US96 e US97](#ssd-us96_e_97)
                        - [SSD US116](#ssd-us116)
			- [(outros SSD arquiteturalmente relevantes)](#outros-ssd-arquiteturalmente-relevantes-1)
		- [Vista de Implementação](#vista-de-implementação)
		- [Vista Física](#vista-física)
	- [Nível 3 (MDR)](#nível-3-mdr)
		- [Vista Lógica](#vista-lógica-2)
		- [Vista de Processos](#vista-de-processos-2)
			- [SD US06](#sd-us06)
			- [(outros SSD arquiteturalmente relevantes)](#outros-ssd-arquiteturalmente-relevantes-2)
		- [Vista de Implementação](#vista-de-implementação-1)
		- [Vista Física](#vista-física-1)
	- [Nível 3 (UI)](#nível-3-ui)
		- [Vista Lógica](#vista-lógica-3)
		- [Vista de Processos](#vista-de-processos-3)
		- [Vista de Implementação](#vista-de-implementação-2)
		- [Vista Física](#vista-física-2)
	- [Nível 3 (MDV)](#nível-3-mdv)
		- [Vista Lógica](#vista-lógica-4)
		- [Vista de Processos](#vista-de-processos-4)
		- [Vista de Implementação](#vista-de-implementação-3)
		- [Vista Física](#vista-física-3)
	- [Nível 3 (Planeamento)](#nível-3-planeamento)
		- [Vista Lógica](#vista-lógica-5)
		- [Vista de Processos](#vista-de-processos-5)
		- [Vista de Implementação](#vista-de-implementação-4)
		- [Vista Física](#vista-física-4)

# Views

## Introduction
De acordo com o abordado nas aulas de ARQSI, será adotada a combinação de dois modelos de representação arquitetural: C4 e 4+1.

O Modelo de Vistas 4+1 [[Krutchen-1995]](References.md#Kruchten-1995) propõe a descrição do sistema através de vistas complementares permitindo assim analisar separadamente os requisitos dos vários stakeholders do software, tais como utilizadores, administradores de sistemas, project managers, arquitetos e programadores. As vistas são deste modo definidas da seguinte forma:

- Vista lógica: relativa aos aspetos do software visando responder aos desafios do negócio;
- Vista de processos: relativa ao fluxo de processos ou interações no sistema;
- Vista de desenvolvimento: relativa à organização do software no seu ambiente de desenvolvimento;
- Vista física: relativa ao mapeamento dos vários componentes do software em hardware, i.e. onde é executado o software;
- Vista de cenários: relativa à associação de processos de negócio com atores capazes de os espoletar.

O Modelo C4 [[Brown-2020]](References.md#Brown-2020)[[C4-2020]](References.md#C4-2020) defende a descrição do software através de quatro níveis de abstração: sistema, contentor, componente e código. Cada nível adota uma granularidade mais fina que o nível que o antecede, dando assim acesso a mais detalhe de uma parte mais pequena do sistema. Estes níveis podem ser equiparáveis a mapas, e.g. a vista de sistema corresponde ao globo, a vista de contentor corresponde ao mapa de cada continente, a vista de componentes ao mapa de cada país e a vista de código ao mapa de estradas e bairros de cada cidade.
Diferentes níveis permitem contar histórias diferentes a audiências distintas.

Os níveis encontram-se definidos da seguinte forma:
- Nível 1: Descrição (enquadramento) do sistema como um todo;
- Nível 2: Descrição de contentores do sistema;
- Nível 3: Descrição de componentes dos contentores;
- Nível 4: Descrição do código ou partes mais pequenas dos componentes (e como tal, não será abordado neste DAS/SAD).

Pode-se dizer que estes dois modelos se expandem ao longo de eixos distintos, sendo que o Modelo C4 apresenta o sistema com diferentes níveis de detalhe e o Modelo de Vista 4+1 apresenta o sistema de diferentes perspetivas. Ao combinar os dois modelos torna-se possível representar o sistema de diversas perspetivas, cada uma com vários níveis de detalhe.

Para modelar/representar visualmente, tanto o que foi implementado como as ideias e alternativas consideradas, recorre-se à Unified Modeling Language (UML) [[UML-2020]](References.md#UML-2020) [[UMLDiagrams-2020]](References.md#UMLDiagrams-2020).

## Nível 1
### Vista Lógica
![N1-VL](diagramas/nivel1/N1-VistaLogica.png)

### Vista de Processos
#### N1 SSD US08
![N1-VP-US08](diagramas/nivel1/N1-VP-US08-RegistarJogador.png)

#### N1 SSD US05
![N1-VP-US05](diagramas/nivel1/N1-VP-US05-AtualizaPerfil.png)

#### N1 SSD Login
![N1-VP-Login](diagramas/nivel1/N1-VP-USa-LoginJogador.png)

## Nível 2

### Vista Lógica

![N2-VL](diagramas/nivel2/Vistalógica.png)

![N2-VL-Auth](diagramas/nivel2/N2-VL_Com_Auth.png)

### Vista de Processos
#### SSD US03

![N2-SSD-US03](diagramas/nivel2/VProcessos-UC3.jpg)

#### SSD US05

![N2-SSD-US05](diagramas/nivel2/VProcessos-UC5.jpg)

#### SSD US06

![N2-SSD-US06](diagramas/nivel2/VProcessos-UC6.jpg)

#### SSD US07

![N2-SSD-US07](diagramas/nivel2/VProcessos-UC7.jpg)

#### SSD US08

![N2-SSD-US08](diagramas/nivel2/VProcessos-UC8.jpg)

#### SSD US09

![N2-SSD-US09](diagramas/nivel2/VProcessos-UC9.jpg)

#### SSD US10

![N2-SSD-US10](diagramas/nivel2/VProcessos-UC10.jpg)

#### SSD US11

![N2-SSD-US11](diagramas/nivel2/VProcessos-UC11.jpg)

#### SSD US12

![N2-SSD-US012](diagramas/nivel2/VProcessos-UC12.jpg)

#### SSD US33

![N2-SSD-US033](diagramas/nivel2/VProcessos-UC33.jpg)

#### SSD US35

![N2-SSD-US035](diagramas/nivel2/VProcessos-UC35.jpg)

### Vista de Implementação

![N2-VI](diagramas/nivel2/VistaImplementação.jpg)

### Vista Física

Vista física com Módulo IA implantado em servidor do departamento.

![N2-VF](diagramas/nivel2/VistaFísica.jpg)

## Nível 3 (MDR)
### Vista Lógica
Alternativa baseada numa arquitetura por camadas sobrepostas:
![N3-VL-MDR-alt1](diagramas/nivel3/MDR/N3-VL-MDR-alt1.png)

Alternativa baseada numa arquitetura por camadas concêntricas (Onion):
![N3-VL-MDR-alt2](diagramas/nivel3/MDR/N3-VL-MDR-alt2.png)

A alternativa Onion será a adotada.

### Vista de Processos

#### SD US03
![UC03](diagramas/nivel3/N3-VP-US03.png)

#### SD US05
![UC05](diagramas/nivel3/N3-VP-US05.png)

#### SD US06
![UC06](diagramas/nivel3/N3-VP-US06.png)

#### SD US08
![UC08](diagramas/nivel3/N3-VP-US08.png)

#### SD US35
![UC35](diagramas/nivel3/N3-VP-US35.png)

#### SD US96 e 97
![UC96_e_97](diagramas/nivel3/N3_VP_US96_e_US97_-_Apresentar_texto_RGPD_e_Dar_Consentimento.png)

#### SD US116
![UC116](diagramas/nivel3/N3_VP_116_Eliminar_conta_RGPD.png)

#### (outros SSD arquiteturalmente relevantes)
[...]



### Vista de Implementação
![N3-VI-MDR-alt2](diagramas/nivel3/MDR/N3-VI-MDR-alt2.png)

Alguns detalhes mais (se existissem pais do que 4 níveis, podia ser considerado nível 4):

![N3.1-VI-MDR-alt2](diagramas/nivel3/MDR/N3.1-VI-MDR-alt2.png)

### Vista Física

![VistaFísica](diagramas/nivel2/VistaFísica.jpg)

## Nível 3 (UI)
### Vista Lógica
TBD

### Vista de Processos
#### US8 - Registar Utilizador
![UC08](diagramas/nivel3/SPA/VistaProcesso/N3-VP-SPA-US8.png)

#### US 6 - Editar Humor
![SPA_US6](diagramas/nivel3/SPA_EditarHumor.png)

#### US 8 - Registar Jogador
![SPA_US8](diagramas/nivel3/N3-VP-SPA-US8.png)

#### US 11 - Pedido Introdução
![SPA_US11](diagramas/nivel3/SPA_PedIntro.png)

#### US 10 - Pesquisar ogador
![SPA_US11](diagramas/nivel3/PesquisarJog.png)

#### US 5  - Editar Perfil Próprio
![SPA_US11](diagramas/nivel3/EditarJog.png)

#### US 81  - Feed de posts de utilizador
![SPA_US81](diagramas/nivel3/SPA_ListarPosts.png)

#### US 96 e 97  - Apresentar texto RGPD e Dar Consentimento
![SPA_US96_e_97](diagramas/nivel3/N3_VP_US96_e_US97_-_Apresentar_texto_RGPD_e_Dar_Consentimento.png)

#### US 116  - Eliminar conta
![SPA_US116](diagramas/nivel3/N3_VP_116_Eliminar_conta_RGPD.png)

### Vista de Implementação
![UC08](diagramas/nivel3/SPA/VistaProcesso/)

### Vista Física
TBD

## Nível 3 (MDV)
### Vista Lógica
![SPA_VLogic_N3](diagramas/nivel3/SPA/SPA_VLogic_N3.jpg)

### Vista de Processos
TBD

### Vista de Implementação
![SPA_VImpl_N3](diagramas/nivel3/SPA/SPA_VImpl_N3.jpg)

![SPA_VL_2_VImpl_N3](diagramas/nivel3/SPA/SPA_VL_2_VImpl_N3.jpg)

### Vista Física
TBD

## Nível 3 (Planeamento)
### Vista Lógica
O grupo não frequentou ALGAV

### Vista de Processos
O grupo não frequentou ALGAV

### Vista de Implementação
O grupo não frequentou ALGAV

### Vista Física
O grupo não frequentou ALGAV